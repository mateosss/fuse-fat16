# Informe lab 4: Sistema de archivos - FUSE

## Parte 1

Antes de empezar a implementar la censura analizamos bien a fondo el código a
partir de la función `fat_fuse_read`, cómo funciona el mecanismo de reserva de
clusters, el `cluster_cache`, el `struct fat_file`, el `struct fat_dir_entry` y
el `full_pread`.

A partir de esto entendimos que la función de más alto nivel en
la que podíamos censurar el buffer era `fat_fuse_read`.

Creamos el archivo `censorship.h` que contiene el array `badwords`
con todas las palabras que debemos censurar.

Luego de la lectura hecha manualmente censuramos el buffer en la función
`fat_fuse_read` realizando un ciclo que recorre el buffer hasta encontrar una
badword y la censura.

A la hora de implementarlo tuvimos algunos problemas con las postcondiciones de
cada ciclo en los casos límites, como por ejemplo a dónde apuntaban las
variables `i` y `j`.

Para comparar cada palabla que encontramos en el buffer con las `badwords`
utilizamos la función `memcmp` y si había que censurarla `memset`.

## Parte 2

### `mknod` syscall

Para esta parte seguimos el consejo de la consigna y primero analizamos cómo
funcionaba `mkdir` y por qué tenia sentido que fuera muy parecida a `mknod`.
Luego de analizarla y terminar de entenderla vimos que prácticamente
necesitábamos la misma función con cambios mínimos.

### `write` syscall

Para escribir un archivo se utiliza la syscall `create` que, cuando no está
implementada,
[FUSE hace un `mknod` (ya implementada) seguido de un `write`](http://libfuse.github.io/doxygen/structfuse__operations.html#a8f5349c22f360bda57ff10d10486d148).

Para implementar `fat_file_reserve_clusters` nos basamos en
`fat_file_preload_clusters`. Lo que hace la primera finción es marcar en disco
la cantidad de clusters necesarios para alojar el archivo y referenciarlos al
`file->cluster_cache`.

Para implementar la siguiente función, `do_fat_file_pwrite`, nos basamos en la
análoga para read (`do_fat_file_pread`) donde el código es prácticamente el
mismo reemplazando read por write.

Un gran problema que tuvimos en esta implementación fue que, por alguna razón,
cuando montábamos y creábamos archivos podíamos leerlos pero, al desmontar y
volver a montar perdíamos el contenido del archivo. Estábamos seguros de que
había un problema de sincronización de la FAT de RAM con la del disco. Sin
embargo, al leer el código no encontramos ninguna falla en este aspecto. Luego,
nos dimos cuenta de que el problema estaba en el `file_size` marcado por la
`dentry` ya que esta última estaba hecha por `mknod` que, al no saber nada
acerca del tamaño del archivo, la seteaba en 0. Solucionamos este problema
sincronizando la `dentry` de RAM con la de disco mediante la función
`fat_write_dir_entry`.

# Extras

## Escritura de archivos de más de dos clusters

Al implementar este extra nos dimos cuenta que no estábamos usando la función
`fat_mark_cluster_used` correctamente en `fat_file_reserve_clusters` ya que
pensábamos que al marcar con `fat_set_next_cluster` el cluster quedaba marcado y
que `fat_next_free_cluster` lo iba a tomar como ocupado, pero no fue ese el
caso, al arreglar esto, este extra funcionó sin necesitar más cambios.
